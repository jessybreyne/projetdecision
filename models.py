from app import db


class Sujet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(120), unique=True)
    description = db.Column(db.String(120))
    auteur = db.Column(db.String(64))
    messages = db.relationship("Message", back_populates="sujet")
    sondages = db.relationship("Sondage", back_populates="sujet")

    def __repr__(self):
        return "<Sujet (%d) %s>" % (self.id, self.titre)

    def to_dict(self):
        return {"titre": self.titre,
                "id": self.id,
                "sondages": [x.id for x in self.sondages],
                "messages": [x.id for x in self.messages]}


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    contenu = db.Column(db.String(200))
    auteur = db.Column(db.String(64))
    id_sujet = db.Column(db.Integer, db.ForeignKey("sujet.id"))
    sujet = db.relationship("Sujet", back_populates="messages")

    def __repr__(self):
        return "<Message (%d) %s>" % (self.id, self.contenu)

    def to_dict(self):
        return {"auteur": self.auteur,
                "contenu": self.contenu}


class Sondage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(200))
    id_sujet = db.Column(db.Integer, db.ForeignKey("sujet.id"))
    sujet = db.relationship("Sujet", back_populates="sondages")
    reponses = db.relationship("Reponse", back_populates="sondage")

    def to_dict(self):
        return {"question": self.question,
                "reponses": {rep.auteur: rep.reponse for rep in self.reponses}
                }

    def __repr__(self):
        return "<Sondage (%d) %s>" % (self.id, self.question)


class Reponse(db.Model):
    auteur = db.Column(db.String(64), primary_key=True)
    reponse = db.Column(db.Boolean)
    id_sondage = db.Column(db.Integer, db.ForeignKey(
        "sondage.id"), primary_key=True)
    sondage = db.relationship("Sondage", back_populates="reponses")

    def __repr__(self):
        return "<Reponse (%d) %s>" % (self.auteur, self.reponse)

    def to_dict(self):
        return {"auteur": self.auteur,
                "reponse": self.reponse}


def get_sujets_all():
    return Sujet.query.all()

def add_sujet(titre, desc):
    suj = Sujet(titre=titre, description= desc)
    db.session.add(suj)
    db.session.commit()
    return suj

def add_message_sujet(id_sujet, contenu, auteur):
    mes = Message(contenu=contenu, auteur=auteur, id_sujet=id_sujet)
    db.session.add(mes)
    db.session.commit()


def add_sondage_sujet(id_sujet, question):
    sondage = Sondage(question=question, id_sujet=id_sujet)
    db.session.add(sondage)
    db.session.commit()


def add_reponse_sondage(id_sondage, auteur, reponse):
    reponse = Reponse(id_sondage=id_sondage, auteur=auteur, reponse=reponse)


def sondage_by_sujet(id_sujet):
    return Sondage.query.filter(Sondage.id_sujet == id_sujet).all()


def get_message(id_message):
    return Message.query.get(id_message)


def get_sujet(id_sujet):
    return Sujet.query.get(id_sujet)


def get_sondage(id_sondage):
    return Sondage.query.get(id_sondage)
