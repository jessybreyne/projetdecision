from flask import jsonify, redirect, request
from app import app
import models


@app.route("/")
def index():
    return redirect("/static/index.html")


@app.route('/sujet/')
def view_sujets():
    """
    get :
        description :
            Récupérer la liste des sujets dans la BD
        responses :
            200 :
                description :
                    Envoie effectué avec succès
                schema :
                [
                {titre : "exTitre", id : "exId"},
                {titre : "titre2", id : "id2"}];
                ]
            404 :
                description :
                    Envoie échoué
    """
    return jsonify([sujet.to_dict() for sujet in models.get_sujets_all()])


@app.route('/sujet/<int:id_sujet>')
def view_sujet(id_sujet):
    """
    get :
        description :
            Récupérer les infos d'un sujet à partir de son id
                - les id de messages ordonnés chronologiquement d'un sujet
                - les id de sondages ordonnés chronologiquement d'un sujet
        responses :
            200 :
                description :
                    Envoie effectué avec succès
                schema :
                    {
                        "titre" : "toto",
                        "messages" : [2,12,33,45,86],
                        "sondages" : [1,2]
                    }
            404 :
                description :
                    Envoie échoué
    """
    return jsonify(models.get_sujet(id_sujet).to_dict())


@app.route('/message/<int:id_message>')
def view_message(id_message):
    """
    get :
        description :
            Récupérer les infos d'un message à partir de son id :
                - nom de l'auteur
                - contenu du message
        responses :
            200 :
                description :
                    Envoie effectué avec succès
                schema :
                    {
                        "auteur" : "Jean-Marc",
                        "contenu" : "Je pense que boire de la bière devrait être obligatoire."
                    }
            404 :
                description :
                    Envoie échoué
    """
    return jsonify(models.get_message(id_message).to_dict())


@app.route('/sondage/<int:id_sondage>')
def view_sondage(id_sondage):
    """
    get :
        description :
            Récupérer les infos d'un sondage à partir de son id :
                - nom de l'auteur
                - contenu du message
        responses :
            200 :
                description :
                    Envoie effectué avec succès
                schema :
                    {
                        "question" : "On fait une soirée jeudi ?",
                        "reponses" : { "Jean" : true, "Toto" : false }
                    }
            404 :
                description :
                    Envoi échoué
    """
    return jsonify(models.get_sondage(id_sondage).to_dict())


@app.route('/addsujet/', methods=['PUT'])
def add_sujet():
    """
    Permet de créer un nouveau sujet
    """
    import json
    donnees = json.loads(request.data)
    print(donnees["titre"])
    suj = models.add_sujet(donnees["titre"],donnees["desc"])

    return jsonify(suj.to_dict())
